
from enum import Enum
from typing import Optional, Tuple, List, Union
import itertools

from ..PILTypes import Size, Location

class Direction(Enum):
    '''General enumeration of directions'''
    CENTER = 0
    NORTH = 1
    SOUTH = 2
    EAST = 4
    WEST = 8
    NORTHEAST = NORTH + EAST
    NORTHWEST = NORTH + WEST
    SOUTHEAST = SOUTH + EAST
    SOUTHWEST = SOUTH + WEST
    EASTWEST = EAST + WEST
    NORTHSOUTH = NORTH + SOUTH
    ALL = NORTH + SOUTH + EAST + WEST

class Anchor(Enum):
    '''Directions used for anchor positions'''
    CENTER = Direction.CENTER.value
    NORTH = Direction.NORTH.value
    SOUTH = Direction.SOUTH.value
    EAST = Direction.EAST.value
    WEST = Direction.WEST.value
    NORTHEAST = Direction.NORTHEAST.value
    NORTHWEST = Direction.NORTHWEST.value
    SOUTHEAST = Direction.SOUTHEAST.value
    SOUTHWEST = Direction.SOUTHWEST.value

class Edge(Enum):
    '''Directions used for edges'''
    NORTH = Direction.NORTH.value
    SOUTH = Direction.SOUTH.value
    EAST = Direction.EAST.value
    WEST = Direction.WEST.value
    TOP = NORTH
    BOTTOM = SOUTH
    RIGHT = EAST
    LEFT = WEST

class Orientation(Enum):
    '''Directions used for an orientation'''
    NONE = Direction.CENTER.value
    EASTWEST = Direction.EASTWEST.value
    X = EASTWEST
    HORIZONTAL = EASTWEST
    NORTHSOUTH = Direction.NORTHSOUTH.value
    Y = NORTHSOUTH
    VERTICAL = NORTHSOUTH
    ALL = Direction.ALL.value
    BOTH = ALL

class Expand(Enum):
    '''Directions used for expanding space'''
    NONE = Direction.CENTER.value
    NO = NONE
    EASTWEST = Direction.EASTWEST.value
    X = EASTWEST
    HORIZONTAL = EASTWEST
    NORTHSOUTH = Direction.NORTHSOUTH.value
    Y = NORTHSOUTH
    VERTICAL = NORTHSOUTH
    ALL = Direction.ALL.value
    BOTH = ALL
    YES = ALL

class Fill(Enum):
    '''Directions used for filling space'''
    NONE = Direction.CENTER.value
    NO = NONE
    EASTWEST = Direction.EASTWEST.value
    X = EASTWEST
    HORIZONTAL = EASTWEST
    NORTHSOUTH = Direction.NORTHSOUTH.value
    Y = NORTHSOUTH
    VERTICAL = NORTHSOUTH
    ALL = Direction.ALL.value
    BOTH = ALL
    YES = ALL

class PackerOperation(Enum):
    '''Bit values for packer operations that freeze the space.'''
    EXPAND = 1
    POSITION = 2

class Parcel:
    '''
    A parcel is a spatial area that contains a single piece of content
    with specified minimum size, an anchor point for the content, the
    axes along which the parcel is allowed to expand, and the axes along
    which the content will expand to fill the parcel.

    It also allows assignment of the final size and final location of
    the parcel, and the final position of the content within the parcel.

    :param content: The content of the parcel (not used by the Parcel
    class itself)
    :param minSize: The minimum size of the content
    :param anchor: The anchor for the parcel's content.
    :param expand: The axes along which the parcel is allowed to expand.
    :param fill: The axes along which the parcel's content is allowed to
    expand.
    '''
    def __init__(
                self: 'Parcel',
                content: object,
                minSize: Size,
                anchor: Optional[Anchor] = Anchor.CENTER,
                expand: Optional[Expand] = Expand.NO,
                fill: Optional[Fill] = Fill.NO,
            ):
        '''Initialize the parcel'''
        self._content = content
        self._minSize = minSize
        self._anchor = anchor
        self._expand = expand
        self._fill = fill
        self._size = None
        self._content_size = None
        self._location = None
        self._position = None

    @property
    def content(self) -> object:
        '''The content of the parcel.'''
        return self._content

    @property
    def anchor(self) -> Anchor:
        '''
        The anchor for the parcel's content.  This can be changed anytime
        before the parcel's final size is set.
        '''
        return self._anchor

    @anchor.setter
    def anchor(self, value: Anchor):
        if self._size is not None:
            raise RuntimeError('Cannot change anchor after parcel has been packed.')
        self._anchor = value

    @property
    def minSize(self) -> Size:
        '''
        The minimum size of the parcel's content.
        '''
        return self._minSize

    @property
    def expand(self) -> Expand:
        '''
        The axes along which the parcel is allowed to expand.  This can
        be changed anytime before the parcel's final size is set.
        '''
        return self._expand

    @expand.setter
    def expand(self, value: Expand):
        if self._size is not None:
            raise RuntimeError('Cannot change expand after parcel has been packed.')
        self._expand = value

    @property
    def fill(self) -> Anchor:
        '''
        The axes along with the parcel's content is allowed to expand.
        This can be changed anytime before the parcel's final size is set.
        '''
        return self._fill

    @fill.setter
    def fill(self, value: Anchor):
        if self._size is not None:
            raise RuntimeError('Cannot change fill after parcel has been packed.')
        self._anchor = value

    @property
    def size(self) -> Optional[Size]:
        '''
        The final size of the parcel.  Initially None, it can be changed
        *once* to assign the parcel's final size.
        '''
        return self._size

    @size.setter
    def size(self, value: Size):
        if self._size is not None:
            raise RuntimeError('Cannot change size after assignment.')
        self._size = value

    @property
    def content_size(self) -> Optional[Size]:
        '''
        The final size of the parcel's content.  Initially None, it can
        be changed *once* to assign the parcel's content's final size.
        '''
        return self._content_size

    @size.setter
    def content_size(self, value: Size):
        if self._content_size is not None:
            raise RuntimeError('Cannot change content_size after assignment.')
        self._content_size = value

    @property
    def location(self) -> Optional[Location]:
        '''
        The location of the parcel.  Initially None, it can be changed
        *once* to assign the parcel's location.
        '''
        return self._location

    @location.setter
    def location(self, value: Location):
        if self._location is not None:
            raise RuntimeError('Cannot change location after assignment.')
        self._location = value

    @property
    def position(self) -> Optional[Location]:
        '''
        The position of the parcel's content.  Initially None, it can be
        changed *once* to assign the parcel's position.
        '''
        return self._position

    @position.setter
    def position(self, value: Location):
        if self._position is not None:
            raise RuntimeError('Cannot change position after assignment.')
        self._position = value

class SpacePacker:
    '''
    This represents a rectangular space, filled with 0 or more Parcels.
    Each Parcel is "packed" along an edge of the space, leaving a gap
    (or "cavity") in the middle.

    The first Parcel to be packed determines the axis of the space -
    horizontal or vertical.

    As long as subsequent Parcels are added to a compatible edge
    (left/right for horizontal, top/bottom for vertical), they continue
    to be packed in.

    As soon as a Parcel is added to an edge that is not along the
    space's axis, then the "cavity" is filled with a new SpacePacker,
    and from that time forward, all Parcels are added to that embedded
    SpacePacker.  For example, if parcels were added to the left, right,
    left, top, left, right, and left sides, in order, they would show up
    in the packer as follows::

        +-+ +-+ : : : : : : : : : +-+
        | | | | : +-----------+ : | |
        | | | | : |     4     | : | |
        | | | | : +-----------+ : | |
        |1| |3| : . . . . . . . : |2|
        | | | | : +-+ +-+ / +-+ : | |
        | | | | : |5| |7| / |6| : | |
        | | | | : +-+ +-+ / +-+ : | |
        | | | | : . . . . . . . : | |
        +-+ +-+ : : : : : : : : : +-+

    The first three are packed into the main SpacePacker, which is
    horizontal because of the first parcel that got packed.  When the
    4th tries to be packed at the top edge, it creates a new SpacePacker
    (delimited by `:`) and packs #4 into the top edge of that, turning
    it vertical.  The next parcel, going left again, gets immediately
    shuffled to the `:` SpacePacker, then that one allocates another
    SpacePacker (delimited by `.`), which is horizontal and can hold the
    fifth, sixth, and seventh parcels.  The next parcel would get packed
    into the space indicated by `/`.

    :param size: The total size into which this SpacePacker's parcels
    will be packed.
    '''

    def __init__(self, size: Optional[Size] = None):
        self._size: Optional[Size] = size
        '''The actual size of this SpacePacker.'''

        self._minSize: Optional[Size] = None
        '''The calculated minimum size of the contents.'''
        self._content_size: Optional[Size] = None
        '''The calculated size of the contents after expansion.'''
        self._expand: Optional[Expand] = None
        '''The desired expansion of the entire content of the packer.'''
        self._location: Optional[Location] = None
        '''The calculated location of the contents after expansion.'''
        self._parcels_before: List[Parcel,...] = []
        '''The parcels "before" the cavity.'''
        self._parcels_after: List[Parcel,...] = []
        '''The parcels "after" the cavity.'''
        self._orientation = None
        '''
        This packer's orientation: `Orientation.HORIZONTAL` or
        `Orientation.VERTICAL`.
        '''
        self._cavity: Optional['SpacePacker'] = None
        '''
        The SpacePacker that occupies this packer's cavity,
        if any.
        '''
        self._frozen: int = 0
        '''
        Once any method that modifies the contained content
        (`_expand_parcels`, `_locate_parcels`, or `pack`) is called,
        this gets set to a non-zero to indicate no parcels can be
        added.  The non-zero value differs based on which method was
        called, and that will also prevent that same method from being
        called again.
        '''

    @property
    def size(self) -> Union[Size,None]:
        '''
        The actual size of this SpacePacker, or `None if not
        assigned.  This must be assigned before `pack` is called.
        '''
        return self._size

    @size.setter
    def size(self, value: Size):
        if self._size:
            raise RuntimeError('Cannot alter size once set.')
        return self._size

    @property
    def parcels_before(self) -> Tuple[Parcel,...]:
        '''
        The parcels that occur "before" the cavity (meaning on the NORTH
        or WEST edge).  New parcels for the appropriate edge are added
        to the end of this list.
        '''
        return tuple(self._parcels_before)

    @property
    def parcels_after(self) -> Tuple[Parcel,...]:
        '''
        The parcels that occur "after" the cavity (meaning on the SOUTH
        or EAST edge).  New parcels for the appropriate edge are added
        to the _beginning_ of this list.
        '''
        return tuple(self._parcels_before)

    @property
    def cavity(self) -> Union['SpacePacker',None]:
        '''
        The contents of the "cavity" or gap between the "before" and
        "after" parcels.  This is either `None` or a SpacePacker object.
        As soon as this has SpacePacker object, all new Parcels added
        to this SpacePacker get passed into the cavity.
        '''
        return self._cavity

    @property
    def minSize(self) -> Union[Size,None]:
        '''
        The minimum size of this SpacePacker's content, or `None`
        if the packer has no content.
        '''
        # `self._minSize` caches the minimum size
        # until `self.add()` clears the value.
        if not self._minSize:
            self._minSize = self._calculateMinimumSize()
        return self._minSize

    @property
    def content_size(self) -> Union[Size,None]:
        '''
        The size of this SpacePacker's content, after expansion.
        This value is calculated by `expandContent`; until then,
        this is `None`.
        '''
        # `self._content_size` caches the content size.
        return self._content_size

    @property
    def location(self) -> Union[Location,None]:
        '''
        The location of this SpacePacker's content, after packing.
        This value is calculated by `pack`; until that is called,
        this will return `None`.
        '''
        # `self._location` caches the content location.
        return self._location

    @property
    def orientation(self) -> Union[Orientation,None]:
        '''
        The orientation of this SpacePacker's content.
        Until the first parcel is added, this is `None`.
        '''
        # `self._location` caches the content location.
        return self._location

    @property
    def expand(self) -> Union[Expand,None]:
        '''
        The overall expansion of this SpacePacker's content, or `None`
        if the packer has no content.
        '''
        # `self._expand` caches the overall expansion
        # until `self.add()` clears the value.
        if not self._expand:
            self._expand = self._calculateExpansion()
        return self._expand

    def add(parcel: Parcel, side: Edge):
        '''
        Add the parcel to the packer.
        * Abort if the packer is frozen.
        * Reset the `self._minSize` and `self._expand` values.
        * If the packer has a defined cavity, add the parcel to the
        cavity.
        * If the packer has no orientation, define the orientation based
        on `side`.
        * If `side` is compatible with the orientation, add the parcel to
        `parcels_before` or `parcels_after`, depending on `side`.
        * Otherwise, create a new SpacePacker for the cavity and add the
        parcel to the cavity.
        '''
        if self._frozen:
            raise RuntimeError('Cannot add parcel after SpacePacker has been packed.')
        self._minSize = None
        self._expand = None
        if not self._cavity:
            self._cavity.add(parcel, side)
        else:
            if side in (Edge.NORTH, Edge.SOUTH):
                orientation = Orientation.NORTHSOUTH
            else:
                orientation = Orientation.EASTWEST
            if self._orientation is None:
                self._orientation = orientation
            if self._orientation == orientation:
                if side & DIRECTION.NORTHWEST:
                    self._parcels_before.append(parcel)
                else:
                    self._parcels_after.insert(0, parcel)
            else:
                self._cavity = SpacePacker()
                self._cavity.add(parcel, side)

    def _calculateMinimumSize(self) -> Union[Size,None]:
        '''
        calculate the minimum size for the space packer.
        Short-circuit out with `None` if no parcels have been added.
        Start with the minimum size of the cavity, if defined,
        or (0,0) otherwise.
        For each parcel in the `before` and `after` lists,
        request the parcel's minimum size.  For horizontal
        packers, add the parcel's minimum width to the packer's
        minimum width, and if the parcel's minimum height is
        greater than the packer's minimum height, use the parcel's
        height instead.  Similarly for vertical packers, swapping
        width and height.
        '''
        if not self._parcels_before and not self._parcels_after:
            return None
        minSize: Size = (0,0)
        if self._cavity:
            minSize = self._cavity.minSize()
        for parcel in itertools.chain(
                    self._parcels_before,
                    self._parcels_after,
                ):
            parcelMinSize: Size = parcel.minSize()
            if self.orientation == Orientation.Horizontal:
                minSize = (
                    minSize[0] + parcelMinSize[0],
                    max(minSize[1], parcelMinSize[1]),
                )
            else:
                minSize = (
                    max(minSize[0], parcelMinSize[0]),
                    minSize[1] + parcelMinSize[1],
                )
        return tuple(minSize)

    def _calculateExpansion(self) -> Union[Expand,None]:
        '''
        Calculate the overall expansion for this space packer.
        If there are no parcels, short-circuit and return `None`.
        If there is a space packer in the cavity, calculate its
        expansion and keep the component that matches this packer's
        orientation.  If there is no cavity, then start with an
        expansion of `Expand.NONE`.
        Iterate over `parcels_before` and `parcels_after`, as long as
        the calculated expansion is not `Expand.BOTH`.
        Fetch the expansion for each parcel and merge it into the
        calculated expansion value: `Expand.NONE` merged with anything
        becomes the other value; anything merged with itself remains
        the same; and otherwise, the expansion becomes `Expand.BOTH`.
        '''
        if not self._parcels_before and not self._parcels_after:
            return None
        expansion: Expand = Expand.NONE
        if self._cavity:
            expansion = Expand(
                self._cavity._calculateExpansion().value
                & self._orientation.value
            )
        for parcel in itertools.chain(
                    self._parcels_before,
                    self._parcels_after
                ):
            if expansion == Expand.BOTH:
                break
            expansion = Expand(
                expansion.value
                | parcel.expand.value
            )
        return expansion

    def _expandParcels(self):
        '''
        Expand the parcels within the SpacePacker to fill its available
        space.  Abort if we've already done this operation, or if we
        don't yet have a size.  While expanding, also calculate the
        total content size for the SpacePacker's parcels (and cavity).
        '''

        # First, calculate how much excess space we have available to
        # expand into.  Then count the number of parcels (counting the
        # cavity, if any, as a single "parcel") who wish to expand in
        # the packer's orientation.
        # 
        # Initialize a "content size" to (0,0).
        # 
        # Then iterate through all the parcels (and the cavity) and
        # distribute the extra space using a semi-even distribution (see
        # below).  Any parcel that wishes to expand crosswise to the
        # packer's orientation grows by the entire excess space in that
        # axis.  Assign the expanded size to the parcel.  Finally, add
        # the parcel's size "along" our orientation to the content size
        # "along" our orientation, and ensure the content size "across"
        # our orientation is big enough to hold the parcel's size
        # "across" our orientation.
        # 
        # The semi-even distribution algorithm:
        # * Inputs: `space`, `count`
        # * Temporary variables: `accumulator`, `expansion`
        # *. Initialize `accumulator` to `(count//2)` before the
        # first parcel.
        # *. For each parcel (or cavity) that wishes to expand "along"
        # our orientation:
        # 1. Set `expansion` to `(space+accumulator)//count`.
        # 2. Then set `accumulator` to `(space+accumulator)%count`.
        # 3. Finally, add `expansion` to the parcel's size in the
        # "along" axis.

        if self.size is None:
            raise RuntimeError('Cannot expand parcels without size defined.')
        if self.frozen & PackerOperation.EXPAND.value:
            raise RuntimeError('Cannot expand parcels twice.')

        excess_space: Size = (
            self.size[0] - self.minSize[0],
            self.size[1] - self.minSize[1]
        )

        expansion_count: int = 0

        for parcel in itertools.chain(
                    self._parcels_before,
                    [self._cavity] if self._cavity else [],
                    self._parcels_after,
                ):
            if parcel.expand.value & self._orientation.value:
                expansion_count += 1

        accumulator = expansion_count//2

        if self._orientation == Orientation.HORIZONTAL:
            along:int = 0
            across:int = 1
        else:
            along:int = 1
            across:int = 0

        content_size: List[int,int] = [0,0]

        for parcel in itertools.chain(
                    self._parcels_before,
                    [self._cavity] if self._cavity else [],
                    self._parcels_after,
                ):
            parcel_size = list(parcel.minSize)
            if parcel.expand.value & self._orientation.value:
                expansion = (
                    (excess_space[along] + accumulator)
                    // expansion_count
                )
                accumulator = (
                    (excess_space[along] + accumulator)
                    % expansion_count
                )
                parcel_size[along] += expansion
            if parcel.expand.value & ~self._orientation.value:
                parcel_size[across] = self.size[across]
            parcel.size = tuple(parcel_size)
            content_size[along] += parcel_size[along]
            content_size[across] = max(
                content_size[across],
                parcel_size[across],
            )

        self.content_size = tuple(content_size)

        self.frozen |= PackerOperation.EXPAND.value

    @staticmethod
    def _positionParcel(parcel: Parcel, location: Location)
        '''
        Position a single parcel:
        Assign the parcel's location to that which was provided.
        Calculate the parcel's content size based on its minimum
        size, it's "fill" values, and its final size.
        Calculate the delta X and delta Y values to position
        the parcel's content within the parcel's final size based
        on the parcel's anchor.
        Finally, assign the parcel's content position to the
        parcel location plus the delta X and delta Y values.
        '''

        parcel.location = location
        content_size = parcel.minSize
        if parcel.fill.value & Fill.X.value:
            content_size = (parcel.size[0], content_size[1])
        if parcel.fill.value & Fill.Y.value:
            content_size = (content_size[0], parcel.size[1])
        delta_x = parcel.size[0] - content_size[0]
        delta_y = parcel.size[1] - content_size[1]
        if parcel.anchor.value & Anchor.WEST.value:
            delta_x = 0
        elif not (parcel.anchor.value & Anchor.EAST.value):
            delta_x /= 2
        if parcel.anchor.value & Anchor.NORTH.value:
            delta_y = 0
        elif not (parcel.anchor.value & Anchor.SOUTH.value):
            delta_y /= 2
        parcel.position = (
            location[0] + delta_x,
            location[1] + delta_y,
        )

    def _position_parcels(self):
        '''
        Position the parcels and cavity based on their size and my
        location.  Recursively positions parcels in the cavity.
        '''

        if self.size is None:
            raise RuntimeError('Cannot position parcels without size defined.')
        if self.location is None:
            raise RuntimeError('Cannot position parcels without location defined.')
        if self.content_size is None:
            raise RuntimeError('Cannot position parcels without content size defined.')
        if self.frozen & PackerOperation.POSITION.value:
            raise RuntimeError('Cannot position parcels twice.')

        location = list(self.location)
        if self.orientation == Orientation.HORIZONTAL:
            along = 0
            across = 1
        else:
            along = 1
            across = 0

        for parcel in itertools.chain(
                    self.parcels_before,
                    [self.cavity] if self.cavity else [],
                    self.parcels_after,
                ):
            parcel_location = list(location)
            if parcel.size[across] < self.size[across]:
                parcel_location[across] += (
                    (self.size[across] - parcel.size[across])//2
                )
            parcel.location = tuple(parcel_location)
            if isinstance(parcel, SpacePacker):
                parcel._position_parcels()
            location[along] += parcel.size[along]

        self.frozen |= PackerOperation.POSITION.value

    def pack(self):
        '''
        Pack all parcels into the space allowed for this packer.
        This first computes the minimum size for the packer, then
        expands all parcels, assigns the location for the packer, and
        finally positions all parcels.
        '''

        if self.size is None:
            raise RuntimeError('Cannot pack without defined size.')
        if self.orientation is None:
            raise RuntimeError('Cannot pack without any parcels.')

        if not (self.frozen & PackerOperation.EXPAND.value):
            self._expandParcels()
        if not (self.frozen & PackerOperation.POSITION.value):
            self.location = (
                (self.size[0] - self.content_size[0])//2,
                (self.size[1] - self.content_size[1])//2,
            )
            real_size = self._size
            self._size = self.content_size
            self._position_parcels()
            self._size = real_size

    def __iter__(self):
        for parcel in itertools.chain(
                    self.parcels_before,
                    iter(self.cavity) if self.cavity else [],
                    self.parcels_after,
                ):
            yield parcel
