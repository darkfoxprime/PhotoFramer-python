
import configparser

_KEY_NOT_FOUND = object()

class StackedConfigParser(configparser.ConfigParser):

    def __init__(self, defaults=None, **kwargs):
        if isinstance(defaults, configparser.RawConfigParser):
            self.parent = defaults
            defaults = None
        else:
            self.parent = None
        super().__init__(defaults=defaults, **kwargs)

    def get(self, section, option, **kwargs):
        kwnofallback = {key:value for key,value in kwargs.items() if key != 'fallback'}
        try:
            return super().get(section, option, **kwnofallback)
        except configparser.NoSectionError as e:
            if not self.parent:
                raise
            return self.parent.get(section, option, **kwargs)
        except configparser.NoOptionError as e:
            if not self.parent:
                raise
            try:
                return self.parent.get(section, option, **kwargs)
            except configparser.NoSectionError as e:
                try:
                    return self.parent.get(self.parent.default_section, option, **kwargs)
                except configparser.NoSectionError as e:
                    raise configparser.NoOptionError(option, section)

    def has_section(self, section):
        return super().has_section(section) or (self.parent.has_section(section) if self.parent else False)

    def options(self, section):
        if self.parent:
            opts = self.parent.options(section)
        else:
            opts = []
        return list(set(opts) + set(super().options(section)))

    def items(self, **kwargs):
        if self.parent:
            try:
                d = self.parent.items(**kwargs)
            except configparser.NoSectionError:
                kwargs_default = dict(kwargs.items())
                kwargs_default['section'] = self.parent.default_section
                d = self.parent.items(**kwargs_default)
        else:
            d = {}
        try:
            d.update(dict(super().items(**kwargs)))
        except configparser.NoSectionError:
            if not self.parent:
                raise
            kwargs_default = dict(kwargs.items())
            kwargs_default['section'] = self.default_section
            d.update(dict(super().items(**kwargs_default)))
        return list(d.items())

    def popitem(self):
        raise NotImplementedError()

    def has_option(self, section, option):
        return super().has_option(section, option) or (self.parent.has_option(section, option) if self.parent else False)

    def set(self, *a, **k):
        raise NotImplementedError()

    def write(self, *a, **k):
        raise NotImplementedError()

    def remove_option(self, *a, **k):
        raise NotImplementedError()

    def remove_section(self, *a, **k):
        raise NotImplementedError()

    def __getitem__(self, *a, **k):
        raise NotImplementedError()

    def __setitem__(self, *a, **k):
        raise NotImplementedError()

    def __delitem__(self, *a, **k):
        raise NotImplementedError()

    def __contains__(self, *a, **k):
        raise NotImplementedError()

    def __len__(self, *a, **k):
        raise NotImplementedError()

    def __iter__(self, *a, **k):
        raise NotImplementedError()
