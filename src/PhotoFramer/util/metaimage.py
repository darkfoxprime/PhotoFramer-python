
from datetime import datetime
from io import IOBase
import os.path
from PIL import Image
from typing import Optional, Union

from ..PILTypes import Size

File = IOBase

class UndefinedImageError(RuntimeError): pass

class MetaImage:
    '''An image with extra metadata.
    '''

    def __init__(
                self: 'MetaImage',
                imageWidth: Optional[int] = None, 
                imageHeight: Optional[int] = None,
                imageName: Optional[str] = None,
                imageFormat: Optional[str] = None,
                sourceImage: Optional['MetaImage'] = None,
            ):
        if all(x is None for x in (imageWidth, imageHeight, imageName, imageFormat)):
            if sourceImage is None:
                self.image = None
            else:
                self.image = sourceImage.image.copy()
                self.format = sourceImage.format
                self.name = sourceImage.name
                self.date = sourceImage.date
        elif any(x is None for x in (imageWidth, imageHeight, imageName, imageFormat)):
            raise ValueError('imageWidth, imageHeight, imageName, and imageFormat must all be None or all non-None.')
        else:
            self.image = Image.new('RGBA', (imageWidth, imageHeight))
            self.format = imageFormat
            self.name = imageName
            self.date = datetime.now()

    @classmethod
    def readImage(
                klass,
                sourceImage: Union[File, str],
                imageName: Optional[str] = None,
            ) -> 'MetaImage':
        '''
        Read an image file and populate the metadata

        :param sourceImage: The name of the file to read, or the file
        pointer to read from, opened in binary read mode
        :param imageName: The name of the image.  If not specified, the
        image name will be constructed from the source image filename,
        or will be `Untitled` if the source image is a file pointer
        :raises PIL.UnidentifiedImageError: if the image type could not
        be identified
        :raises OSError: if the file could not be found or an I/O error
        occurred while reading the file
        '''
        print(repr(sourceImage))
        mi = klass()
        mi.image = Image.open(sourceImage)
        mi.image.load()
        mi.format = mi.image.format
        exif_date = mi.image.getexif().get(Image.ExifTags.Base.DateTime, None)
        if exif_date is not None:
            exif_date = datetime.strptime(exif_date, '%Y:%m:%d %H:%M:%S')
        else:
            exif_date = datetime.fromtimestamp(
                os.stat(sourceImage).st_mtime
            )
        mi.date = exif_date
        if imageName is None:
            if isinstance(sourceImage, str):
                imageName = ' '.join(
                    word.capitalize()
                    for word in (
                        os.path.splitext(
                            os.path.basename(sourceImage)
                        )[0]
                        .split('_')
                    )
                )
            else:
                imageName = 'Untitled'
        mi.name = imageName
        return mi

    def saveImage(imageFile: Union[File, str]):
        '''
        Save the image to a file

        :param imageFile: The name of the file to save, or the file
        pointer to save to, opened in binary write mode
        :raises ValueError: if the file format is unknown
        :raises OSError: if an I/O error occurred while saving the file
        '''
        if self._image is None:
            raise UndefinedImageError('Cannot save an undefined image.')
        save_args = {}
        if self.format == 'JPEG':
            save_args['exif'] = self._image.getexif()
        self._image.convert('RGB').save(imageFile, format=self.format, **save_args)

    @property
    def image(self) -> Optional[Image.Image]:
        '''Return the underlying image'''
        return self._image

    @image.setter
    def image(self, value: Optional[Image.Image]):
        '''Replace the underlying image with a new image'''
        self._image = value

    @property
    def name(self) -> Optional[str]:
        '''Return the image name'''
        if self._image is not None:
            return self._name
        return None

    @name.setter
    def name(self, value: str):
        '''Set the image name'''
        if self._image is None:
            raise UndefinedImageError('Cannot change name on an undefined image.')
        self._name = value

    @property
    def size(self) -> Union[Size,None]:
        '''Return the image size'''
        if self._image is not None:
            return self._image.size
        return None

    @property
    def format(self) -> Optional[str]:
        '''Return the image format'''
        if self._image is not None:
            return self._format
        return None

    @format.setter
    def format(self, value: str):
        if self._image is None:
            raise UndefinedImageError('Cannot change format on an undefined image.')
        self._format = value
        self._image.format = value

    @property
    def date(self) -> Optional[datetime]:
        '''Return the datetime for the image'''
        if self._image is not None:
            exif = self._image.getexif()
            if Image.ExifTags.Base.DateTime in exif:
                return datetime.strptime(exif[Image.ExifTags.Base.DateTime], '%Y:%m:%d %H:%M:%S')
        return None

    @date.setter
    def date(self, value: Optional[datetime]):
        '''Modify the datetime for the image'''
        if self._image is None:
            raise UndefinedImageError('Cannot change date on an undefined image.')
        self._image.getexif()[Image.ExifTags.Base.DateTime] = (
            value.strftime('%Y:%m:%d %H:%M:%S')
        )
        self._date = value
