'''Type annotation definitions for PIL types.'''

from typing import Union, Tuple

GrayColor = Union[int,float]
RGBColor = Union[Tuple[int,int,int],Tuple[float,float,float]]
RGBAColor = Union[Tuple[int,int,int,int],Tuple[float,float,float,float]]
PILColor = Union[GrayColor,RGBColor,RGBAColor]

Position = Tuple[int,int]
Size = Tuple[int,int]
Rectangle = Tuple[int,int,int,int]
