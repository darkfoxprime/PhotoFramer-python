"""Command Line Interface for the PhotoFramer application"""

import argparse
import datetime
import sys
from typing import Optional, Sequence

from .__VERSION__ import APPLICATION, VERSION


def parse_args(args: Optional[Sequence[str]] = None, desc_before: Optional[str] = None, desc_after: Optional[str] = None) -> argparse.Namespace:

    argparser = argparse.ArgumentParser(description = desc_before, epilog = desc_after, prog = APPLICATION)

    argparser.add_argument(
                '--version',
                action = 'version',
                version = f'''{APPLICATION} {VERSION}''',
                help = '''Display the program version and exit.'''
            )
    argparser.add_argument(
                '--artist', '-A',
                type = str,
                help = '''Override any artist properties found with this name.'''
            )
    argparser.add_argument(
                '--date', '-D',
                type = datetime.date.fromisoformat,
                help = '''Override the date the photograph was taken with this date, in ISO-8601 format (e.g. YYYY-MM-DD)'''
            )
    argparser.add_argument(
                '--style', '-S',
                type = str,
                help = '''Override the chosen framing style from any property files with this style.'''
            )
    argparser.add_argument(
                '--propertyfile', '-p',
                type = str,
                help = '''Use this property file name instead of "frame.prop"'''
            )
    argparser.add_argument(
                '--imagepropertyext', '--propertyext', '--extension', '-e',
                type = str,
                help = '''Use this extension to locate the image-specific property file instead of ".prop"'''
            )
    argparser.add_argument(
                '--output', '-o',
                type = str,
                default = '''%D%S%B.framed.%X''',
                help = '''Save the image using this filename.  The following wildcards are available, and must be used when more than one input file is being processed.  %%B - the base name of the input file, without the extension.  %%D - the directory which contains the input file.  %%S - the pathname separator (/ or \ as appropriate).  %%X - the extension of the input file.  The default is "%%D%%S%%B.framed.%%X"'''
            )
    argparser.add_argument(
                'input_file',
                type = str,
                nargs = '+',
                help = '''Original image file or files to process.'''
            )


    if args is None:
        args = sys.argv[1:]
    if args:
        args = argparser.parse_args()
    else:
        argparser.print_help()
        sys.exit(0)

    return args

def main():
    args = parse_args()
    import pprint
    pprint.pprint(vars(args))
