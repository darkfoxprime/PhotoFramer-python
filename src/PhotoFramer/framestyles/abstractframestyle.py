'''Abstract base class defining the `FrameStyle` interface.'''

from abc import ABC, abstractmethod
from configparser import ConfigParser
from deprecation import deprecated
import importlib
from PIL import ImageColor, ImageFont
from typing import Optional,Tuple

from ../PILTypes import RGBColor

from ../metaimage import MetaImage

class FrameStyleInterface(ABC):
    '''This is the "interface" that all frame styles must implement.'''

    @deprecated
    def processImage(
                self: 'FrameStyle',
                sourceImage: file,
                framedImage: file,
                options: ConfigParser,
            ) -> None:
        '''Loads an image from the source file, processes it according
        to the given options, and saves it to the framed file.

        :param sourceImage: The file containing the image to process
        :param framedImage: The file to which the processed image should be saved
        :param options: The options to be used when processing the image
        :raises ValueError: if an option has an invalid value
        :raises NameError: if a required option is not found
        :raises IOError: if an error occurs while loading or saving the image
        '''

        sourceMetaImage = MetaImage.readImage(sourceImage)
        framedMetaImage = self.processImage(sourceMetaImage, options)
        framedMetaImage.save(framedImage)

    @abstractmethod
    def processImage(
                self: 'FrameStyle',
                sourceImage: MetaImage,
                options: ConfigParser,
            ) -> MetaImage:
        '''Returns a framed image from a source image and set of options.

        :param sourceImage: The image to process
        :param options: The options to be used when processing the image
        :raises ValueError: if an option has an invalid value
        :raises NameError: if a required option is not found
        :returns: The processed image
        :rtype: MetaImage
        '''

        return sourceImage

class AbstractFrameStyle(FrameStyleInterface):
    '''The base class for all other frame styles.

    This implements a number of convenience functions for handling
    various common option types.
    '''

    @property
    def styleName(self: FrameStyleInterface) -> str:
        '''The name of the style, derived from the class name.
        If the class name is not of the format `FrameStyle...`,
        raise a ValueError.

        :returns: the name of the style implemented by the given
        object
        :raises ValueError: if the object's class name does not
        comply with the required format
        '''

        styleName = getattr(self.__class__, '_styleName', None)
        if styleName is None:
            if not self.__class__.__name__.startswith('FrameStyle'):
                raise ValueError("Invalid class name")
            styleName = self.__class__.__name__[10:]
            setattr(self.__class__, '_styleName', styleName)
        return styleName

    def getOption(
                self: FrameStyleInterface,
                options: ConfigParser,
                optionName: str,
                defaultValue: str,
            ) -> str:
        '''Fetch a named option.  First try looking up the option name
        in the options section named for the current framing style.  If
        that does not find the option, try looking up the option name
        prepended by the current framing style and an underscore (`_`)
        in the default section.  If the option name is still not found,
        it will return `defaultValue`.

        For example, in the framing style class `FrameStyleTest`,
        calling this method for the option `color` will first look up
        `color` in the `Test` options section, then will try looking up
        `Test_color` in the default section, and finally will return the
        default value if no other value was found.

        :param options: The options for the current image
        :param optionName: The option name to look up
        :param defaultValue: The default value to return if the option
        is not found
        :raises ValueError: if the frame style object's class name does
        not comply with the required format
        '''

        try:
            return options.get(self.styleName, optionName)
        except (NoSectionError,NoOptionError):
            return options.get(options.default_section, f'''{self.styleName}_{optionName}''', defaultValue)

    def parseOptionAsColor(
                self: FrameStyleInterface,
                options: ConfigParser,
                optionName: str,
                defaultValue: Union[str,RGBColor],
            ) -> RGBColor:
        '''Parse an option as a PIL color, using the `ImageColor` module.

        :param options: The image's options
        :param optionName: The option name to look up
        :param defaultValue: The default value if the option is not
        found.  This can be specified either as a string or as a PIL
        color
        :returns: the color parsed from the option value or default
        value
        :raises ValueError: if the option value or default value are not
        valid colors
        '''
        option = self.getOption(options, optionName, defaultValue)
        if isinstance(option, str):
            option = ImageColor.getrgb(option)
        return option

    def parseOptionAsFont(
                self: FrameStyleInterface,
                options: ConfigParser,
                optionName: str,
                defaultValue: Union[str,ImageFont],
            ) -> ImageFont:
        ''''Parse an option as a font, using the `ImageFont` module.

        :param options: The image's options
        :param optionName: the option name to look up
        :param defaultValue: The default value if the option is not
        found.  This can be specified either as a string or as a PIL
        font
        :returns: the font parsed from the option value or default
        value
        :raises OSError: if the font was not found or not readable
        '''
        option = self.getOption(options, optionName, defaultValue)
        if isinstance(option, str):
            font_resource = importlib.resources.files('..').joinpath(option)
            with as_file(font_resource) as font_file:
                if font_resource.endswith('.ttf'):
                    option = ImageFont.truetype(font_file)
                else:
                    option = ImageFont.load(font_file)
        return option

    def parseOptionAsInteger(
                self: FrameStyleInterface,
                options: ConfigParser,
                optionName: str,
                defaultValue: Union[str,int],
            ) -> int:
        ''''Parse an option as an integer.

        :param options: The image's options
        :param optionName: the option name to look up
        :param defaultValue: The default value if the option is not
        found.  This can be specified either as a string or an integer
        :returns: the integer value of the option
        :raises ValueError: if the string cannot be parsed as an integer
        '''
        option = self.getOption(options, optionName, defaultValue)
        if isinstance(option, str):
            option = int(option, base=0)
        return option

    def parseOptionAsPercentage(
                self: FrameStyleInterface,
                options: ConfigParser,
                optionName: str,
                defaultValue: Union[str,int],
            ) -> int:
        ''''Parse an option as a percentage (an integer between 0 and
        100, inclusive, followed by a `%`).

        :param options: The image's options
        :param optionName: the option name to look up
        :param defaultValue: The default value if the option is not
        found.  This can be specified either as a string or an integer
        :returns: the integer value of the option
        :raises ValueError: if the string cannot be parsed as a
        percentage
        '''
        option = self.getOption(options, optionName, defaultValue)
        if isinstance(option, str):
            if not option.endswith('%'):
                raise ValueError('Not a valid percentage', option)
            try:
                optval = int(option[:-1])
            except ValueError:
                raise ValueError('Not a valid percentage', option)
            if optval < 0 or optval > 100:
                raise ValueError('Not a valid percentage', f'''{optval}%''')
            option = optval
        return option

    def parseOptionAsIntegerOrPercentage(
                self: FrameStyleInterface,
                options: ConfigParser,
                optionName: str,
                defaultValue: Union[str,int],
            ) -> int:
        ''''Parse an option as a percentage (an integer between 0 and
        100, inclusive, followed by a `%`) or a non-negative integer.

        :param options: The image's options
        :param optionName: the option name to look up
        :param defaultValue: The default value if the option is not
        found.  This can be specified either as a string or an integer
        :returns: the integer value of the option.  If the option was
        a percentage, the percentage is returned as a negative value
        (-1 through -100)
        :raises ValueError: if the string cannot be parsed as a
        percentage or integer
        '''
        option = self.getOption(options, optionName, defaultValue)
        if isinstance(option, str):
            if option.endswith('%'):
                optval = int(option[:-1])
                if optval < 0 or optval > 100:
                    raise ValueError('Not a valid percentage', f'''{optval}%''')
                option = optval
            else:
                option = int(option, base=0)
        return option

    def parseOptionAsBoolean(
                self: FrameStyleInterface,
                options: ConfigParser,
                optionName: str,
                defaultValue: Union[str,bool],
            ) -> bool:
        ''''Parse an option as a boolean value.  The return value is
        `True` if the option value is one of `True`, `Yes`, or `On`,
        ignoring case; or `False` if the option value is one of `False`,
        `No`, or `Off`.  A `ValueError` is raised for any other option
        value.

        :param options: The image's options
        :param optionName: the option name to look up
        :param defaultValue: The default value if the option is not
        found.  This can be specified either as a string or a boolean
        :returns: the boolean value of the option
        :raises ValueError: if the string is not one of the values
        `True`, `False`, `Yes`, `No`, `On`, or `Off, ignoring case
        '''
        option = self.getOption(options, optionName, defaultValue)
        if isinstance(option, str):
            optval = option.lower()
            if optval in ('true', 'yes', 'on'):
                option = True
            else if optval in ('false', 'no', 'off'):
                option = False
            else:
                raise ValueError('Not a valid boolean', optval)
        return option
